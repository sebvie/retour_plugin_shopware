<?php

namespace RetourPlugin\Services;

class Dhl
{
    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $portalId;

    /**
     * @var string
     */
    private $deliveryName;

    /**
     * @var string
     */
    private $end_point = "https://amsel.dpwn.net/abholportal/gw/lp/SoapConnector";

    /**
     * @param string $dhlUsername
     * @param string $dhlPassword
     * @param string $dhlPortalId
     * @param string $dhlDeliveryName
     * @param string $dhlStreet
     * @param string $dhlNumber
     * @param string $dhlFirstname
     * @param string $dhlLastname
     * @param string $dhlZip
     * @param string $dhlCity
     * @return null|string
     */
    public function getRetourePdf(
        $dhlUsername,
        $dhlPassword,
        $dhlPortalId,
        $dhlDeliveryName,
        $dhlStreet,
        $dhlNumber,
        $dhlFirstname,
        $dhlLastname,
        $dhlZip,
        $dhlCity
    )
    {
        $this->username = $dhlUsername;
        $this->password = $dhlPassword;
        $this->portalId = $dhlPortalId;
        $this->deliveryName = $dhlDeliveryName;
        $pdf = null;
        $xmlRequest = $this->getRequestXml($dhlFirstname, $dhlLastname, $dhlStreet,$dhlNumber, $dhlZip, $dhlCity);
        $response = $this->curlSoapRequest($xmlRequest);
        if($response){
            $pdf = $this->getPdfFromResponse($response);
        }
        return $pdf;
    }

    /**
     * @param string $pdfData
     * @return string
     * @throws \Exception
     */
    public function savePdfAsFile($pdfData){
        $path = sys_get_temp_dir();
        if(file_put_contents($path,$pdfData)){
            return $path;
        }
        else
        {
            throw new \Exception('could not save pdf');
        }
    }

    /**
     * @param $pdf
     * @return void
     */
    public function displayPdf($pdf){
        header("Content-type: application/pdf");
        echo $pdf;
    }

    /**
     * @param string $first_name
     * @param string $last_name
     * @param string $street
     * @param string $streetNumber
     * @param string $zip
     * @param string $city
     * @return string
     */
    private function getRequestXml($first_name, $last_name, $street, $streetNumber, $zip, $city)
    {
        $first_name = htmlspecialchars($first_name);
        $last_name = htmlspecialchars($last_name);
        $street = htmlspecialchars($street);
        $streetNumber = htmlspecialchars($streetNumber);
        $zip = htmlspecialchars($zip);
        $city = htmlspecialchars($city);
        $request =
              "<?xml version='1.0' encoding='UTF-8' ?>
                 <soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:var='https://amsel.dpwn.net/abholportal/gw/lp/schema/1.0/var3bl'>
                     <soapenv:Header>
                         <wsse:Security soapenv:mustUnderstand='1' xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'>
                             <wsse:UsernameToken>
                                <wsse:Username>".$this->username."</wsse:Username>
                                <wsse:Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'>
                                    ".$this->password."
                                </wsse:Password>
                            </wsse:UsernameToken>
                        </wsse:Security>
                    </soapenv:Header>
                    <soapenv:Body>
                        <var:BookLabelRequest
                        portalId='".$this->portalId."'
                        deliveryName='".$this->deliveryName."'
                        shipmentReference='Shipment Reference'
                        customerReference='Customer Reference'
                        labelFormat='PDF'
                        senderName1='$first_name'
                        senderName2='$last_name'
                        senderCareOfName='CareofName'
                        senderContactPhone=''
                        senderStreet='$street'
                        senderStreetNumber='$streetNumber'
                        senderBoxNumber=''
                        senderPostalCode='$zip'
                        senderCity='$city' />
                    </soapenv:Body>
                </soapenv:Envelope>";
        return $request;
    }

    /**
     * @param string $xmlRequest
     *
     * @return mixed
     * @throws \Exception
     */
    private function curlSoapRequest($xmlRequest)
    {
        $header = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "Content-length: " . strlen($xmlRequest),
        );

        $soap_do = curl_init();
        curl_setopt($soap_do, CURLOPT_URL, $this->end_point);
        curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($soap_do, CURLOPT_TIMEOUT, 10);
        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($soap_do, CURLOPT_POST, true);
        curl_setopt($soap_do, CURLOPT_POSTFIELDS, $xmlRequest);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER, $header);
        $response = curl_exec($soap_do);


        if (!$response) {
            $err = 'Curl error: ' . curl_error($soap_do);
            throw new \Exception($err);
        }
        curl_close($soap_do);
        return $response;
    }

    /**
     * @param $response
     * @return string
     */
    private function getPdfFromResponse($response)
    {
        $xml = simplexml_load_string($response);
        /*        dump($xml);*/
        $ns = $xml->getNamespaces(true);
        /*        dump($ns);*/
        $soap = $xml->children($ns['env']);
        /*        dump($soap);*/
        $pdf = $soap->Body->children($ns['var3bl'])->BookLabelResponse->label;

        $pdf = base64_decode($pdf);
        return $pdf;
    }
}