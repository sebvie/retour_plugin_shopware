<?php

class Shopware_Controllers_Backend_Retour extends Shopware_Controllers_Api_Rest
{

    /**
     * @throws Exception
     * @return void
     */
    public function sendMailAction()
    {
        $customer_id = (int)$this->Request()->getParam('customer_id');
        $order_id = (int)$this->Request()->getParam('order_id');
        $sentToCustomer = $this->Request()->getParam('sentToCustomer') == 'true';
        $sentToShopOwner = $this->Request()->getParam('sentToShopOwner') == 'true';
        $sentToCustomAdress = $this->Request()->getParam('customSender') == 'true';

        if(!$sentToCustomer && !$sentToShopOwner && !$sentToCustomAdress){
            throw new Exception('please choose a receiver');
        }

        $retoureData = $this->getRetourData($customer_id, $order_id);
        $pluginConfig = $this->container->get('shopware.plugin.cached_config_reader')->getByPluginName('RetourPlugin');

        $pdf = $this->getRoutePdf(
            $pluginConfig['dhlUsername'],
            $pluginConfig['dhlPassword'],
            $pluginConfig['dhlPortalId'],
            $pluginConfig['dhlDeliveryName'],
            $retoureData->getStreet(),
            $retoureData->getStreetNumber(),
            $retoureData->getFirstName(),
            $retoureData->getLastName(),
            $retoureData->getZip(),
            $retoureData->getCity()
        );

        if($sentToCustomer){
            $this->sendRetourMail(
                $pluginConfig['dhlCustomFileName'],
                $pluginConfig['dhlCustomMailSubject'],
                $pluginConfig['dhlCustomMailTextOne'],
                $pluginConfig['dhlCustomMailTextTwo'],
                $retoureData->getUserSalutation(),
                $retoureData->getUserFirstname(),
                $retoureData->getUserLastname(),
                $order_id,
                $retoureData->getUserEmail(),
                $pdf
            );
            echo 'eMail erfolgreich an Kunden verschickt: '.$retoureData->getUserEmail().'<br>';
        }

        if($sentToShopOwner){
            if(!$pluginConfig['dhlShopMail'] || $pluginConfig['dhlShopMail'] == ''){
                throw new Exception('no shop email set');
            }
            $this->sendRetourMail(
                $pluginConfig['dhlCustomFileName'],
                $pluginConfig['dhlCustomMailSubject'],
                $pluginConfig['dhlCustomMailTextOne'],
                $pluginConfig['dhlCustomMailTextTwo'],
                $retoureData->getUserSalutation(),
                $retoureData->getUserFirstname(),
                $retoureData->getUserLastname(),
                $order_id,
                $pluginConfig['dhlShopMail'],
                $pdf);
            echo 'eMail erfolgreich an Shop-Bereiber verschickt: '.$pluginConfig['dhlShopMail'].'<br>';
        }

        if($sentToCustomAdress){

            if(!$pluginConfig['dhlCustomMail'] || $pluginConfig['dhlCustomMail'] == ''){
                throw new Exception('no shop email set');
            }

            $this->sendRetourMail(
                $pluginConfig['dhlCustomFileName'],
                $pluginConfig['dhlCustomMailSubject'],
                $pluginConfig['dhlCustomMailTextOne'],
                $pluginConfig['dhlCustomMailTextTwo'],
                '',
                $pluginConfig['dhlCustomFirstname'],
                $pluginConfig['dhlCustomLastname'],
                $order_id,
                $pluginConfig['dhlCustomMail'],
                $pdf
            );

            echo 'Custom eMail erfolgreich verschickt an: '.$pluginConfig['dhlCustomMail'].'<br>';
        }
    }

    /**
     * @param int $userID
     * @param int $orderID
     * @return RetourPlugin\Entity\RetoureData
     * @throws Exception
     */
    protected function getUserData($userID, $orderID){
        $retoureData = new \RetourPlugin\Entity\RetoureData();

        $this->setShippingAddressData($userID, $orderID, $retoureData);

        $this->setAddressData($retoureData);

        $this->setUserData($retoureData);

        return $retoureData;
    }

    /**
     * @param int $order_id
     * @param string $email
     * @param string $pdf
     * @return void
     */
    protected function sendRetourMail($filename, $subject, $mail_text_1, $mail_text_2, $salutation, $lastname,
                                      $firstname, $order_id, $email, $pdf){
        /** @var \Shopware\Components\Plugin\ConfigReader $configReader  */
        $transport = $this->container->get('shopware.mail_transport');

        if($salutation != ''){
            if($salutation == 'mr'){
                $salutation = 'Herr';
            }else{
                $salutation = 'Frau';
            }
        }

        $shopname = Shopware()->Config()->Shopname;
        $company = Shopware()->Config()->get('company');
        $shop_adress = Shopware()->Config()->get('address');

        $mail_text_1 = str_replace('%%salutation%%',$salutation,$mail_text_1);
        $mail_text_1 = str_replace('%%firstname%%',$firstname,$mail_text_1);
        $mail_text_1 = str_replace('%%lastname%%',$lastname,$mail_text_1);
        $mail_text_2 = str_replace('%%order_id%%',$order_id,$mail_text_2);
        $filename = str_replace('%%order_id%%',$order_id,$filename);

        $text = $mail_text_1.'
        '.$mail_text_2.'
        '.$shopname.'
        '.$company.'
        '.$shop_adress;

        $html = '<h3>'.$mail_text_1.'</h3></br>'.
            '<p>'.$mail_text_2.'</p></br>'.
            '</br>'.
            '<p>'.$shopname.'</p></br>'.
            '<p>'.$company.'</p></br>'.
            '<p>'.$shop_adress.'</p>';

        $mail = new Zend_Mail('UTF-8');

        $subject = str_replace('%%shopname%%',Shopware()->Config()->Shopname,$subject);
        $subject = str_replace('%%order_id%%',$order_id,$subject);

        $mail->setFrom(Shopware()->Config()->Mail, Shopware()->Config()->Shopname);
        $mail->setSubject($subject);
        $mail->setBodyText($text);
        $mail->setBodyHtml($html);
        $mail->addTo($email);

        $mail->createAttachment(
            $pdf,
            Zend_Mime::TYPE_OCTETSTREAM,
            Zend_Mime::DISPOSITION_ATTACHMENT,
            Zend_Mime::ENCODING_BASE64,
            $filename.'.pdf'
        );

        $mail->Send($transport);
    }

    /**
     * @param int $customer_id
     * @param int $order_id
     *
     * @return \RetourPlugin\Entity\RetoureData
     * @throws Exception
     */
    protected function getRetourData($customer_id, $order_id)
    {

        if(!$customer_id || !$order_id){
            throw new Exception(sprintf('customer_id %s and order_id %s should be integers',
                    $customer_id,
                    $order_id
                )
            );
        }

        $retoureData = $this->getUserData($customer_id, $order_id);
        if($retoureData->getCountryId() != 2){
            throw new Exception('nur in Deutschland ist Retoure möglich');
        }

        return $retoureData;
    }

    /**
     *
     * @param $dhlUsername
     * @param $dhlPassword
     * @param $dhlPortalId
     * @param $dhlDeliveryName
     * @param $dhlStreet
     * @param $dhlNumber
     * @param $dhlFirstname
     * @param $dhlLastname
     * @param $dhlZip
     * @param $dhlCity
     * @return string
     * @throws Exception
     */
    protected function getRoutePdf(
        $dhlUsername,
        $dhlPassword,
        $dhlPortalId,
        $dhlDeliveryName,
        $dhlStreet,
        $dhlNumber,
        $dhlFirstname,
        $dhlLastname,
        $dhlZip,
        $dhlCity
    )
    {
        /** @var RetourPlugin\Services\Dhl $dhl */
        $dhl = $this->get('service.retour.dhl');

        if($dhlNumber === NULL){$dhlNumber = 0;}

        $pdf = $dhl->getRetourePdf(
            $dhlUsername,
            $dhlPassword,
            $dhlPortalId,
            $dhlDeliveryName,
            $dhlStreet,
            $dhlNumber,
            $dhlFirstname,
            $dhlLastname,
            $dhlZip,
            $dhlCity
        );
        if(!$pdf){
            throw new Exception('dhl service error');
        }

        return $pdf;
    }

    /**
     * @param \RetourPlugin\Entity\RetoureData $retoureData
     * @return void
     * @throws Exception
     */
    protected function setUserData($retoureData)
    {
        if(!is_int($retoureData->getUserID())){
            throw new Exception('userId missing');
        }
        $sql_s_user =
            sprintf("
                SELECT 
                  email
                FROM s_user as t
                WHERE t.id = %d
                ",
                $retoureData->getUserID()
        );

        /** @var Zend_Db_Statement_Pdo $sql_s_user */
        $res_user = Shopware()->Db()->query($sql_s_user);
        if($res_user != '' && !empty($res_user)){
            $res_user = $res_user->fetch();

            $retoureData->setUserEmail($res_user['email']);
        }
        else{
            throw new Exception('No user address!');
        }
    }

    /**
     * @param \RetourPlugin\Entity\RetoureData $retoureData
     * @return void
     * @throws Exception
     */
    protected function setAddressData($retoureData)
    {
        if(!is_int($retoureData->getUserID())){
            throw new Exception('userId missing');
        }

        $sql_s_user_useraddresses =
            sprintf(
                "
                SELECT 
                  salutation,
                  firstname,
                  lastname
                FROM s_user_addresses as t
                WHERE t.id = %d
                ",
                $retoureData->getUserID()
            );

        /** @var Zend_Db_Statement_Pdo $res_user_useraddresses */
        $res_user_addresses = Shopware()->Db()->query($sql_s_user_useraddresses);
        if($res_user_addresses != '' && !empty($res_user_addresses)){
            $res_user_addresses = $res_user_addresses->fetch();

            $retoureData->setUserFirstname($res_user_addresses['firstname']);
            $retoureData->setUserLastname($res_user_addresses['lastname']);
            $retoureData->setUserSalutation($res_user_addresses['salutation']);
        }
        else{
            throw new Exception('No user address!');
        }
    }

    /**
     * @param integer $userID
     * @param integer$orderID
     * @param \RetourPlugin\Entity\RetoureData $retoureData
     * @return void
     * @throws Exception
     */
    protected function setShippingAddressData($userID, $orderID, $retoureData)
    {
        if(!is_int($userID)){
            throw new Exception('userId missing');
        }

        if(!is_int($orderID)){
            throw new Exception('orderId missing');
        }

        $sql_s_user_shippingaddress =
            sprintf("
                SELECT 
                  *
                FROM s_order_shippingaddress as t
                WHERE t.userID = %d
                AND t.orderID = %d
                ",
                $userID,
                $orderID
        );

        /** @var Zend_Db_Statement_Pdo $res_user_shippingaddress */
        $res_user_shippingaddress = Shopware()->Db()->query($sql_s_user_shippingaddress);
        if($res_user_shippingaddress != '' && !empty($res_user_shippingaddress)){
            $res_user_shippingaddress = $res_user_shippingaddress->fetch();

            $retoureData->setUserID($userID);
            $retoureData->setCompany($res_user_shippingaddress['company']);
            $retoureData->setSalutation($res_user_shippingaddress['salutation']);
            $retoureData->setFirstName($res_user_shippingaddress['firstname']);
            $retoureData->setLastName($res_user_shippingaddress['lastname']);
            $retoureData->setStreet($res_user_shippingaddress['street']);
            $retoureData->setZip($res_user_shippingaddress['zipcode']);
            $retoureData->setCity($res_user_shippingaddress['city']);
            $retoureData->setCountryId($res_user_shippingaddress['countryID']);
            $retoureData->setStreetAdditional($res_user_shippingaddress['additional_address_line1']);
            $retoureData->setTitle($res_user_shippingaddress['title']);
        }
        else{
            throw new Exception('No shipping address found');
        }
    }
}