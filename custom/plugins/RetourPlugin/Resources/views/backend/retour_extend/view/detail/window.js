//{block name="backend/order/view/detail/window" append}
//{namespace name=backend/order/view/main}
Ext.define('Shopware.apps.RetourPlugin.retour_extend.view.detail.Window', {
    override: 'Shopware.apps.Order.view.detail.Window',
    createTabPanel: function () {

        var me = this;
        var tabPanel = me.callParent(arguments);

        html = true;
        htmlToShow = me.updateTab();

        if(html !== false && me.record){

            tabPanel.add(Ext.create('Shopware.apps.RetourPlugin.view.detail.Retour', {
                title: 'Retour Plugin',
                id: 'smaTab',
                historyStore: me.historyStore,
                record: me.record,
                orderStatusStore: me.orderStatusStore,
                paymentStatusStore:  me.paymentStatusStore,
                smartPackageHtml: htmlToShow,
                logo : "none"
            }));
        }
        return tabPanel;
    },
    updateTab: function(){
        console.log("updateTab");
        return '';
    }

});

Ext.define('Shopware.apps.RetourPlugin.view.detail.Retour', {
    extend: 'Ext.container.Container',
    padding: 10,
    title: 'Retour',

    initComponent:function () {
        var me = this;

        me.items = [
            {
                xtype: 'fieldset',
                title: 'Empfänger',
                defaults: me.defaults,
                items: [
                    {
                        fieldLabel: 'Empfänger',
                        helpText: 'Email wird verschickt an:',
                        xtype: 'checkboxgroup',
                        name: 'retourCheck',
                        id: 'retourCheck',
                        columns: 1,
                        vertical:true,
                        items:
                            [
                                { boxLabel: 'Kunde', id: 'customerCheck', name: 'customer', checked: true },
                                { boxLabel: 'Shop', id: 'ownerCheck', name: 'owner' },
                                { boxLabel: 'Eigener Sender (s. Plugin-settings)', id: 'customSenderCheck', name: 'customSender' }
                            ]
                    }
                ]
            },
            me.createFieldSet()
        ];
        me.title = 'Retour Plugin';
        me.callParent(arguments);
    },

    createFieldSet: function() {
        var me = this,
            title = 'DHL Retour';

        return Ext.create('Ext.form.FieldSet', {
            title: title,
            defaults: {
                labelWidth: 155,
                labelStyle: 'font-weight: 700;'
            },
            layout: 'anchor',
            minWidth:250,
            items: me.createButtons()
        });
    },

    createButtons: function() {
        var me = this,
            buttonText = 'Send',
            containerText = 'Send retour PDF via email.';

        me.button = Ext.create('Ext.button.Button', {
            style: 'margin: 8px 0;',
            cls: 'small primary',
            text: buttonText,
            handler: function() {
                me.sendId();
            }
        });
        me.container = Ext.create('Ext.container.Container', {
            style: 'color: #999; font-style: italic; margin: 0 0 15px 0;',
            html: containerText
        });

        return [me.container,me.button];
    },

    sendId: function(){
        var me = this,
            checkedRecipients = Ext.getCmp('retourCheck').getChecked(),
            sentToCustomer = false,
            customSender = false,
            sentToShopOwner = false;
        Ext.each(checkedRecipients || [], function(checkedRecipient) {
            if(checkedRecipient.name === 'owner'){
                sentToShopOwner = true;
            }
            if(checkedRecipient.name === 'customer'){
                sentToCustomer = true;
            }
            if(checkedRecipient.name === 'customSender'){
                customSender = true;
            }
        }, this);

        Ext.Ajax.request({
            url: '{url module=backend controller=Retour action=sendMail}',
            method: 'GET',
            params : {
                order_id: me.record.get('id'),
                customer_id: me.record.data.customerId,
                sentToCustomer: sentToCustomer,
                sentToShopOwner: sentToShopOwner,
                customSender: customSender
            },
            success: function(e){
                console.log(e);
                var res = e.responseText;
                var first_two = res.substring(0,2);
                if(first_two == '[]'){
                    Ext.Msg.alert('Erfolgreich versendet', res.substring(2));
                }else{
                    Ext.Msg.alert('Erfolgreich versendet', e.responseText);
                }

            },
            failure: function(e){
                console.log(e);
                Ext.Msg.alert('Fehler beim Versenden!', e.responseText);
            }
        });
    }
});
//{/block}