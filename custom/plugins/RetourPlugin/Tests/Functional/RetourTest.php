<?php

use RetourPlugin\Services\Dhl;
/**
 * Created by Sebastian Viereck IT-Services
 * www.sebastianviereck.de
 * Date: 10.12.17
 * Time: 22:10
 */
class RetourTest extends Shopware\Components\Test\Plugin\TestCase
{
    /**
     * @var array
     */
    protected static $ensureLoadedPlugins = [
        'RetourPlugin' => [
            /*'some_config' => 'foo'*/
        ]
    ];

    /**
     * @return void
     */
    public function testGeneratePdf()
    {
        $service = new Dhl();
        $pdf = $service->getRetourePdf(
            'fistName',
            'lastName',
            'street',
            'number',
            '12345',
            'city'

        );
        $this->assertNotEmpty($pdf);
    }
}